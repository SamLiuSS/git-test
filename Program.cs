﻿using System;

namespace git_demo
{
    class Program
    {
        static void Main(string[] args)
        {
            // 求各數平方值
            // 2nd line comment
            for (var j = 0; j < 100; j++)
            {
                // add 3rd line comment
                var i = j * j ;
                Console.WriteLine($"i = {i}");
            }
        }
    }
}
